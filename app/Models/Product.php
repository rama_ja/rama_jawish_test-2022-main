<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        return $this->hasMany(Inventory:: Class,'product_id');
    }
    public function image()
    {
        return $this->hasOne(Image:: Class,'o_id')->where('o_type','product');
    }

    public function getTotalQuantityAttribute()
    {
        $total=0;
        foreach ($this->units as $pro){
            $total+=$pro->amount * $pro->unit->modifier;
        }
        return $total;
    }

    public function getImagePathAttribute($var)
    {
        return $this->attributes['image_path']=$var;

    }


}
